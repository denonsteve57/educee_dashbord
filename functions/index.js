// const functions = require("firebase-functions");

// var admin = require("firebase-admin/app");

// admin.initializeApp(functions.config().firebase);


const functions = require("firebase-functions");

var admin = require("firebase-admin/app");

admin.initializeApp(functions.config().firebase);



// // Create and deploy your first functions
// // https://firebase.google.com/docs/functions/get-started
//
exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", {structuredData: true});
  response.send("Hello from Firebase!");
});



exports.test = functions.firestore
.document('eleves/{userId}')
.onCreate((snap,context)=> {

  var token_parent = "dY8dB2ApT12QwPE26E7pvq:APA91bGO8C1M0s4RZYh4dhkE9TgxlvzXq8x32yGzX4ToWQGWqqnl4HahzrcaHGUEodnG40jTPgw5tMcINTgwjiPEpkdBezQA_5W1AyUHH9anKzV9WxoFiCZmBWBz5AnOHhuWn7g0AtFQ"

  var payload = {
    notification: {
      title: "Nouvelle elve",
      body: "educ'ee vous informe souhaite la bienvenue"
    }
  }

  var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  }

  // enregistrement et envoie de la notification
  var now = new Date();
  var annee   = now.getFullYear();
  var mois    = now.getMonth() + 1;
  var jour    = now.getDate();
  var heure   = now.getHours();
  var minute  = now.getMinutes();
  var seconde = now.getSeconds();
  var date = jour+"/"+mois+"/"+annee;
  var heure = heure+"h"+minute+"m"+seconde+'s';

  //admin.messaging().sendToTopic()
  return admin.messaging().sendToTopic(token_parent, payload, options).then(
    (success)=> {
       admin.firestore().collection('notifications').add({date:date, heure:heure, payload})
    }
  )


})



// Les notifications parents

exports.nouvelleNote = functions.firestore
.document('notes/{userId}')
.onCreate((snap,context)=>{

  //recuperation des informations entrée
  const id_eleve = snap.data().id_eleve
  const matiere = snap.data().matiere
  const type_evaluation = snap.data().type_evaluation
  const valeur = snap.data().valeur

  //recuperation des informations sur l'élève à partir de son id
  admin.firestore().collection("eleves").doc(id_eleve).get().then(
    (querySnapchot)=> {
      // infos sur son parent
      admin.firestore().collection('parents').where('email','==', querySnapchot.data().email_parent).get().then(
        (querySnapchot2)=> {
           //recupération du token du parent et envoie de la notification
          var token_parent;
          querySnapchot2.forEach(doc=> {
            token_parent = doc.data().notification_token

            var payload = {
              notification: {
                title: "Nouvelle note",
                body: "educ'ee vous informe que votre enfant"+querySnapchot.data().nom+' '+querySnapchot.data().prenoms+' '+'a reçu la note de'+valeur+' '+'en'+' '+matiere+' '+' pour'+' '+type_evaluation
              }
            }

            var options = {
              priority: "high",
              timeToLive: 60 * 60 * 24
            }

            // enregistrement et envoie de la notification
            var now = new Date();
            var annee   = now.getFullYear();
            var mois    = now.getMonth() + 1;
            var jour    = now.getDate();
            var heure   = now.getHours();
            var minute  = now.getMinutes();
            var seconde = now.getSeconds();
            var date = jour+"/"+mois+"/"+annee;
            var heure = heure+"h"+minute+"m"+seconde+'s';

            //admin.messaging().sendToTopic()
            return admin.messaging().sendToTopic(token_parent, payload, options).then(
              (success)=> {
                 admin.firestore().collection('notifications').add({date:date, heure:heure, payload})
              }
            )

          })
        }
       
        
      )
    }
  )
})

