import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestionelevesPageRoutingModule } from './gestioneleves-routing.module';

import { GestionelevesPage } from './gestioneleves.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestionelevesPageRoutingModule
  ],
  declarations: [GestionelevesPage]
})
export class GestionelevesPageModule {}
