import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionelevesPage } from './gestioneleves.page';

const routes: Routes = [
  {
    path: '',
    component: GestionelevesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionelevesPageRoutingModule {}
