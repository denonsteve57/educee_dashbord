import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { IonModal } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-gestioneleves',
  templateUrl: './gestioneleves.page.html',
  styleUrls: ['./gestioneleves.page.scss'],
})
export class GestionelevesPage implements OnInit {
  @ViewChild(IonModal)
  modal!: IonModal;
  message = 'This modal example uses triggers to automatically open a modal when the button is clicked.';
  name: string | undefined;

  dataEcole = {
    nom: "",
    prenoms: "",
    classe: "",
    nom_ecole: "",
    matricule: "",
    email_parent: ""
  }


  listeeleves: any[] = [];
  etat: string | undefined;
 
  constructor(private firestore: AngularFirestore,
              private router: Router ) {
    //this.firestore.firestore.useEmulator("localhost", 8080 );
    this.recuperationListeEleve();
   }

  ngOnInit() {
  }


  cancel() {
    this.modal.dismiss(null, 'cancel');
  }

  recuperationListeEleve() {
    this.firestore.firestore.collection('eleves').onSnapshot(
      (querySnapchot)=> {
      if(!querySnapchot.empty) {
        this.listeeleves = []
        querySnapchot.forEach(doc=> {
          this.listeeleves.push(
            {
              nom: doc.data()['nom'],
              prenoms: doc.data()['prenoms'],
              classe: doc.data()['classe'],
              nom_ecole: doc.data()['nom_ecole'],
              matricule: doc.data()['matricule'],
              email_parent: doc.data()['email_parent'],
              id_eleve: doc.id
            }
          )
        })
        this.etat = "non_vide"
      }
      if(querySnapchot.empty) {
        this.etat = "vide"
      }
      }
    )
  }

  confirm() {
    this.modal.dismiss(this.name, 'confirm');
    console.log(this.dataEcole)
    this.firestore.firestore.collection('eleves').add(this.dataEcole).then(
      (success)=>{
        Swal.fire({
          title: "Opération Réussie",
          text: "Vous venez d'ajouter un nouvelle élève",
          icon: "success",
          heightAuto: false
        })
      }, (erreur)=> {
        Swal.fire({
          title: "Echec de l'opération",
          text: "L'élève n'as pas été ajouter",
          icon: "error",
          heightAuto: false
        })
      }
    )
  }

  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<string>>;
    if (ev.detail.role === 'confirm') {
      this.message = `Hello, ${ev.detail.data}!`;
    }
  }


  supprimerEleve(item: any) {
    Swal.fire(
      {
        title: "ATTENTION",
        text: "Voulez Vous vraiment supprimer l'élève "+item.nom+" "+item.prenoms,
        icon:"warning",
        heightAuto: false,
        showDenyButton: true
      }
    ).then(
      (result)=> {
        if(result.value) {
          console.log("supprimer")
          this.firestore.firestore.collection("eleves").doc(item.id_eleve).delete().then(
            (success)=> {
              Swal.fire({
                title:"Suppression réussi",
                text: "L'élève a été supprimer avec succes",
                icon: 'success',
                heightAuto: false
              })
            },(error)=> {

            }
          )
        }
        if(!result.value) {
          console.log("annuler la ")
          Swal.fire({
            title:"Suppression annulé",
            text: "La suppression à été anulée",
            icon: "error",
            heightAuto: false
          })
        }
      }
    )
  }



  gestionNotes(item: any) {
    console.log(item);
    localStorage.setItem('infos_eleve', JSON.stringify(item))
    this.router.navigate(['gestionnotes'])
  }

}
