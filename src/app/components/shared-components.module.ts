import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideComponent } from './side/side.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SideComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ],
  exports: [SideComponent]
})
export class SharedComponentsModule { }
