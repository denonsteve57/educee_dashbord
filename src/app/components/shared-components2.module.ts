import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { Side2Component } from './side2/side2.component';



@NgModule({
  declarations: [Side2Component],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ],
  exports: [Side2Component]
})
export class SharedComponents2Module { }
