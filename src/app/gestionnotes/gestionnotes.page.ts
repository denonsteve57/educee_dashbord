import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { info } from 'console';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-gestionnotes',
  templateUrl: './gestionnotes.page.html',
  styleUrls: ['./gestionnotes.page.scss'],
})
export class GestionnotesPage implements OnInit {
  @ViewChild('popover') popover: any;

  @ViewChild('popover2') popover2: any;
  
  matieres: any = [
    // "FRANCAIS",
    // "MATHEMATIQUES",
    // "SVT",
    // "PCT",
    // "ANGLAIS",
    // "HISTOIRE-GEOGRAPHIE"
  ] 


  isOpen = false;

  isOpen2 = false;

  matiere={
    nom: "",
  }

  type_evaluation={
    nom:""
  }

  infos_eleve: any;

 note1 = {
  valeur: "",
  matiere: "",
  type_evaluation: "note1",
  classe:"",
  id_eleve: "",
 }

 note2 = {
  valeur: "",
  matiere: "",
  type_evaluation: "note2",
  classe:"",
  id_eleve: "",
 }

 note3 = {
  valeur: "",
  matiere: "",
  type_evaluation: "note3",
  classe:"",
  id_eleve: "",
 }

 devoir = {
  valeur: "",
  matiere: "",
  type_evaluation: "devoir",
  classe:"",
  id_eleve: "",
 }

 compo = {
  valeur: "",
  matiere: "",
  type_evaluation: "composition",
  classe:"",
  id_eleve: "",
 }

 note = {
  date:"",
  num_note: 0,
  valeur: "",
  matiere: "",
  type_evaluation: "",
  classe:"",
  id_eleve: "",
 }

 level: string;

 ismatiere: string;

 isevaluation: string;

 evaluations: any = []



  constructor(private firestore: AngularFirestore) { 
    this.level = "single-eleve";
    this.ismatiere = "chargement";
    this.isevaluation = "chargement"
    this.infos_eleve = localStorage.getItem('infos_eleve');
    this.infos_eleve = JSON.parse(this.infos_eleve);
    console.log(this.infos_eleve);
  }

  ngOnInit() {
    this.recuperationMatiereGeneral();
   // this.recuperationTypeEvaluation();
   this.numeroterNote();
  }

  numeroterNote() {
    this.firestore.firestore.collection("notes").where('id_eleve', '==', this.infos_eleve.id_eleve).get().then(
      (reponse)=> {
       // alert(reponse.size)
       if(reponse.size === 0) {
        this.note.num_note = 1
       } else {
        this.note.num_note = reponse.size + 1
       }

      }
    )
  }
  
  recuperationMatiereGeneral() {
    this.firestore.firestore.collection('matieres').onSnapshot(
      (querySnapchot)=> {
        if(querySnapchot.empty) {
          this.ismatiere = "false"
        }
        if(!querySnapchot.empty) {
          this.matieres = []
          this.ismatiere = "true"
          querySnapchot.forEach(doc=> {
            this.matieres.push(doc.data()["nom"])
          })
        }
      }
    )
  }

  // recuperationTypeEvaluation() {
  //   this.firestore.firestore.collection("type_evaluation").onSnapshot(
  //     (querySnapchot)=> {
  //       if(querySnapchot.empty) {
  //         this.isevaluation = "false"
  //       }
  //       if(!querySnapchot.empty) {
  //         this.evaluations = []
  //         this.isevaluation = "true";
  //         querySnapchot.forEach(doc=> {
  //           this.evaluations.push(doc.data()["nom"])
  //         })
  //       }
  //     }
  //   )
  // }

  enregistrerNote(item: any) {
    if(this.note.valeur && this.note.type_evaluation) {
      this.note.matiere = item;
      this.note.id_eleve = this.infos_eleve.id_eleve;
      this.note.classe = this.infos_eleve.classe;
      this.firestore.firestore.collection('notes').add(this.note).then(
        (success)=> {
          Swal.fire({
            title:"Opération réussi",
            icon: "success",
            heightAuto: false
          });
          this.note = {
            date:"",
            num_note: 0,
            valeur: "",
            matiere: "",
            type_evaluation: "",
            classe:"",
            id_eleve: "",
           }

           this.numeroterNote();

        }, (error)=> {
          Swal.fire({
            title:"Echec de l'opération",
            icon: "error",
            heightAuto: false
          });
        }
      )
    } else {
      // Attention a renseigner tout les champs

    }
  }


 async valider(item: any) {
    console.log(item)
    if(this.note1.valeur !== "") {
      this.note1.matiere = item
      this.note1.id_eleve = this.infos_eleve.id_eleve;
      this.note1.classe = this.infos_eleve.classe
      this.firestore.firestore.collection("notes").add(this.note1).then(
        (success)=> {
          Swal.fire({
            title:"Opération réussi",
            icon: "success",
            heightAuto: false
          })
        }, (error)=> {

        }
      )
    }
    if(this.note2.valeur !== "") {
      this.note2.matiere = item
      this.note2.id_eleve = this.infos_eleve.id_eleve;
      this.note2.classe = this.infos_eleve.classe
      this.firestore.firestore.collection("notes").add(this.note2).then(
        (sussess)=> {
          Swal.fire({
            title:"Opération réussi",
            icon: "success",
            heightAuto: false
          })
        },
        (error)=> {

        }
      )
    }
    if(this.note3.valeur !== "") {
      this.note3.matiere = item
      this.note3.id_eleve = this.infos_eleve.id_eleve;
      this.note3.classe = this.infos_eleve.classe
      this.firestore.firestore.collection("notes").add(this.note3).then(
        (success)=> {
          Swal.fire({
            title:"Opération réussi",
            icon: "success",
            heightAuto: false
          })
        }, 
        (error)=> {

        }
      )
    }
    if(this.devoir.valeur !== "") {
      this.devoir.matiere = item
      this.devoir.id_eleve = this.infos_eleve.id_eleve;
      this.devoir.classe = this.infos_eleve.classe
      this.firestore.firestore.collection("notes").add(this.devoir).then(
        (success)=>{
          Swal.fire({
            title:"Opération réussi",
            icon: "success",
            heightAuto: false
          })
        }, (error)=> {

        }
      )
    }
    if(this.compo.valeur !== "") {
      this.compo.matiere = item
      this.compo.id_eleve = this.infos_eleve.id_eleve;
      this.compo.classe = this.infos_eleve.classe
      this.firestore.firestore.collection("notes").add(this.compo).then(
        (success)=> {
          Swal.fire({
            title:"Opération réussi",
            icon: "success",
            heightAuto: false
          })
        }, (error)=> {

        }
      )
    }
   
  }

  presentPopover(e: Event) {
    this.popover.event = e;
    this.isOpen = true;
  }

  presentPopover2(e: Event) {
    this.popover2.event = e;
    this.isOpen2 = true;
  }


  ajouterMatiere() {
    this.level = "add-matiere";

  }

  onKeyPress(event: any) {
    console.log(event); 
    //alert("enter pressed")
    if(this.matiere.nom) {
      this.firestore.firestore.collection("matieres").where('nom', '==', this.matiere.nom).get().then(
        (querySnapchot)=> {
          if(querySnapchot.empty) {
           this.firestore.collection("matieres").add(this.matiere).then(
            (success)=>{
              this.isOpen = false;
              this.matiere.nom = ""
              Swal.fire(
                {
                  text:"Matiere ajoutée avec succes",
                  icon:"success",
                  heightAuto: false
                }
              )
            },(echec)=> {
              this.isOpen = false;
              Swal.fire(
                {
                  text:"Echec de l'ajout, veuillez réssayez",
                  icon:"error",
                  heightAuto: false
                }
              )
            }
           )
          }
          if(!querySnapchot.empty) {
            Swal.fire(
              {
                text:"Cette matière est déja dans la base de données",
                icon:"info",
                heightAuto: false
              }
            )
          }
        }
      )
    } else {
      Swal.fire({
        text:"Veuillez entrer une matière avant de valider !!!",
        icon:"info",
        heightAuto: false
      })
    }
    
  }

  onKeyPress2(event:any) {
    if(this.type_evaluation.nom) {
      this.firestore.firestore.collection('type_evaluation').where('nom', '==', this.type_evaluation.nom).get().then(
        (querySnapchot)=> {
          if(querySnapchot.empty) {
            this.firestore.firestore.collection('type_evaluation').add(this.type_evaluation).then(
              (success)=> {
                this.isOpen2 = false;
                this.type_evaluation.nom = ""
                Swal.fire(
                  {
                    text:"Type d'évaluation ajoutée avec succes",
                    icon:"success",
                    heightAuto: false
                  }
                )
              }, (echec)=> {
                this.isOpen2 = false;
                Swal.fire(
                  {
                    text:"Echec de l'ajout, veuillez réssayez",
                    icon:"error",
                    heightAuto: false
                  }
                )
              }
            )
          }
          if(!querySnapchot.empty) {
            Swal.fire(
              {
                text:"Ce type d'évaluation est déja dans la base de données",
                icon:"info",
                heightAuto: false
              }
            )
          }
        }
      )
    } else {
      Swal.fire({
        text:"Veuillez renseigner un type d'evaluation avant de valider !!!",
        icon:"info",
        heightAuto: false
      })
    }
  }

 

}
