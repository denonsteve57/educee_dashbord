import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestionnotesPageRoutingModule } from './gestionnotes-routing.module';

import { GestionnotesPage } from './gestionnotes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestionnotesPageRoutingModule
  ],
  declarations: [GestionnotesPage]
})
export class GestionnotesPageModule {}
