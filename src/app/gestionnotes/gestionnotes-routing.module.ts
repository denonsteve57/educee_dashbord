import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionnotesPage } from './gestionnotes.page';

const routes: Routes = [
  {
    path: '',
    component: GestionnotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionnotesPageRoutingModule {}
