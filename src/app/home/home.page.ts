import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  level: string

  constructor(private router: Router) {
    this.level = 'auth'
  }

  connexion() {
    this.router.navigate(['menu'])
  }

  Inscription() {
    this.router.navigate(['menu'])
  }

  Admin() {
    this.router.navigate(['menu2'])
  }
}
