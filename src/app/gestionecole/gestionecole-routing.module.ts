import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionecolePage } from './gestionecole.page';

const routes: Routes = [
  {
    path: '',
    component: GestionecolePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionecolePageRoutingModule {}
