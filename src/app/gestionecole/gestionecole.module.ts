import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestionecolePageRoutingModule } from './gestionecole-routing.module';

import { GestionecolePage } from './gestionecole.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestionecolePageRoutingModule
  ],
  declarations: [GestionecolePage]
})
export class GestionecolePageModule {}
