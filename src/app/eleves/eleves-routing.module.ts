import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElevesPage } from './eleves.page';

const routes: Routes = [
  {
    path: '',
    component: ElevesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElevesPageRoutingModule {}
