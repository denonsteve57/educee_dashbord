import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElevesPageRoutingModule } from './eleves-routing.module';

import { ElevesPage } from './eleves.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElevesPageRoutingModule
  ],
  declarations: [ElevesPage]
})
export class ElevesPageModule {}
