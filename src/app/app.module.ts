import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuPageModule } from './menu/menu.module';
import { Menu2PageModule } from './menu2/menu2.module';


import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const firebaseConfig = {
  apiKey: "AIzaSyC5UwoA3opMbxXnBQ-NJpJiR1aG1Y3NLL8",
  authDomain: "educee-3c240.firebaseapp.com",
  projectId: "educee-3c240",
  storageBucket: "educee-3c240.appspot.com",
  messagingSenderId: "23678970918",
  appId: "1:23678970918:web:4591c3adee7774ea038ae7",
  measurementId: "G-91X7VPE394",
   // Update the Firebase configuration to use the emulators
  // Set the host and port for each emulator that you have configured in firebase.json
  emulatorHost: 'localhost',
  emulatorAuthPort: 9099,
  emulatorFirestorePort: 8080,
  emulatorFunctionsPort: 5001,
};

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule,
     IonicModule.forRoot(),
      AppRoutingModule,
       MenuPageModule, 
       Menu2PageModule,
             // 3. Initialize
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule,
    NgbModule,
      ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
