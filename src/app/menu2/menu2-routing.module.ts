import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menu2Page } from './menu2.page';

const routes: Routes = [
  { path: '', redirectTo: 'gestionecole', pathMatch: 'full' },
  {
    path: '',
    component: Menu2Page,
    children: [
      {
        path: 'gestionecole',
        loadChildren: () => import('../gestionecole/gestionecole.module').then( m => m.GestionecolePageModule)
      },
      {
        path: 'gestioneleves',
        loadChildren: () => import('../gestioneleves/gestioneleves.module').then( m => m.GestionelevesPageModule)
      },
      {
        path: 'gestionnotes',
        loadChildren: () => import('../gestionnotes/gestionnotes.module').then( m => m.GestionnotesPageModule)
      },
     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Menu2PageRoutingModule {}
