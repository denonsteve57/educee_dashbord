import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Menu2PageRoutingModule } from './menu2-routing.module';

import { Menu2Page } from './menu2.page';
import { SharedComponents2Module } from '../components/shared-components2.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Menu2PageRoutingModule,
    SharedComponents2Module
  ],
  declarations: [Menu2Page]
})
export class Menu2PageModule {}
