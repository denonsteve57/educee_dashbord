import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  { path: '', redirectTo: 'notes', pathMatch: 'full' },
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: "notes",
        loadChildren: ()=> import('../notes/notes.module').then(m=>m.NotesPageModule)
      },
      {
        path: 'eleves',
        loadChildren: () => import('../eleves/eleves.module').then( m => m.ElevesPageModule)
      },
    ]
  }
];

// const routes: Routes = [
//   { path: '', redirectTo: 'emmetre-offre', pathMatch: 'full' },
//   {
//     path: '',
//     component: MenuPage,
//     children: [
//       {
//         path: 'emmetre-offre',
//         loadChildren: () => import('../employeur/emmetre-offre/emmetre-offre.module').then(m => m.EmmetreOffrePageModule)
//        },
//        {
//         path: 'postulant',
//         loadChildren: () => import('../employeur/postulant/postulant.module').then( m => m.PostulantPageModule)
//       },
//     ]
//   }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
